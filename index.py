class Regards():
    def __init__(self):
        print('Loading python program')

    def hola(self):
        self.__init__()
        toPath = [
            [5, 4, 3, 5],
            [2, 6, 1, 6],
            [7, 8, 2, 3]
        ]
        getLastPath = toPath.pop(len(toPath) - 1)
        print(getLastPath)
        toPath.insert(1, getLastPath)
        print(toPath)

Regards.hola('hola')